(ns atompit.cards.chart
  (:require [sablono.core :as sab]
            [devcards.core :refer-macros [defcard defcard-doc]]

            [atompit.fe.chart :as chart]))

(def c3data
  {:json {:data1 [30 200 100 400 150 250 250]
          :data2 [150 320 50 40 55 75 80]}
   :groups []
   :axis {:x {:type :category
              :categories ["Mon" "Tue" "Wed" "Thu" "Fri" "Sat" "Sun"]}}})

(defcard "This is our starting data" c3data)

(defcard
  "*Line* chart"
  (chart/Chart c3data))

(defcard
  "*Area* chart"
  (chart/Chart (assoc c3data :type :area)))

(defcard
  "***Stacked** Area* chart"
  (chart/Chart (assoc c3data
                 :groups [[:data1 :data2]]
                 :types {:data1 :area :data2 :area}
                 :labels true)))

(def moredata
  {:json {:week1-1 [30 200 100 400 150 250 250]
          :week1-2 [150 320 50 40 55 75 80]
          :week2-1 [20 180 120 300 180 220 210]
          :week2-2 [100 220 100 90 85 65 180]}
   :groups [[:week1-1 :week1-2] [:week2-1 :week2-2]]})

(defcard "Then we add another pair" moredata)

(defcard
  "*Double **Stacked** Area* chart"
  (chart/Chart (assoc moredata
                 :type :area)))
