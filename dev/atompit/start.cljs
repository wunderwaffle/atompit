(ns atompit.start
  (:require [atompit.fe :as fe]
            [figwheel.client :as figwheel :include-macros true]))

(figwheel/watch-and-reload
  :websocket-url (str "ws://" js/location.hostname ":3449/figwheel-ws")
  :jsload-callback (fn []
                     (fe/trigger-render)))
