(ns atompit.devcards
  (:require [atompit.fe :as fe]
            [figwheel.client :as figwheel :include-macros true]
            [sablono.core :as sab]
            [devcards.core :refer-macros [defcard]]

            [atompit.cards.chart]))

(figwheel/watch-and-reload
  :websocket-url (str "ws://" js/location.hostname ":3449/figwheel-ws"))

(devcards.core/start-devcard-ui!)

(defcard cacacard
  (sab/html [:h1 "test"]))
