.PHONY: dev version

LEIN ?= lein
SASSC ?= sassc

SASSC-exists := $(shell $(SASSC) --version 2>/dev/null)
ifndef SASSC-exists
$(error 'sassc' is not installed!)
endif

dev:
	rlwrap $(LEIN) run -- --dev

prod:
	$(LEIN) run -- --min
	lein uberjar
	rsync -Pc uber/atompit-*-standalone.jar cup:/opt/atompit.jar
