(defproject atompit "0.1.0-SNAPSHOT"
  :description "Mine your data, see your data"

  :source-paths #{"src"}

  :dependencies
  [[org.clojure/clojure "1.7.0"]
   [org.clojure/clojurescript "1.7.170"]
   [devcards "0.2.1"
    :exclusions [cljsjs/react
                 cljsjs/react-dom]]

   [cljsjs/react "0.14.3-0"]
   [cljsjs/react-dom "0.14.3-1"]
   [cljsjs/react-dom-server "0.14.3-0"]
   [sablono "0.5.0"]
   [rum "0.6.0"]
   [rum-reforms "0.4.3"]
   [hoplon/javelin "3.8.4"]
   [cljsjs/c3 "0.4.10-0"]
   [com.cognitect/transit-cljs "0.8.232"]

   [org.clojure/tools.nrepl "0.2.12"]
   [com.cemerick/piggieback "0.2.1"]
   [cider/cider-nrepl "0.10.0"]
   [refactor-nrepl "2.0.0-SNAPSHOT"]
   [figwheel-sidecar "0.5.0-2" :scope "test"]

   [jarohen/phoenix.runtime "0.1.2"
    :exclusions [com.keminglabs/cljx
                 org.clojure/tools.reader
                 org.clojure/tools.namespace
                 medley]]
   [org.clojure/tools.cli "0.3.3"]
   [org.clojure/tools.namespace "0.3.0-alpha1"]
   [medley "0.7.0"]
   [hawk "0.2.5"]

   [com.andrewmcveigh/cljs-time "0.3.14"
    :exclusions [org.clojure/clojurescript]]
   [clj-time "0.11.0"]

   [me.raynes/conch "0.8.0"
    :exclusions [org.clojure/tools.macro
                 org.clojure/tools.reader]]

   [io.pedestal/pedestal.service "0.4.1"
    :exclusions [org.clojure/tools.reader]]
   [io.pedestal/pedestal.immutant "0.4.1"]
   [ring "1.4.0"
    :exclusions [org.clojure/tools.reader]]
   [hiccup "1.0.5"]

   [com.datomic/datomic-free "0.9.5344"
    :exclusions [org.slf4j/slf4j-nop]]
   [org.clojure/java.jdbc "0.4.2"
    :exclusions [mysql/mysql-connector-java
                 org.apache.derby/derby
                 hsqldb
                 org.xerial/sqlite-jdbc
                 net.sourceforge.jtds/jtds]]
   [org.postgresql/postgresql "9.3-1103-jdbc4"]

   [ch.qos.logback/logback-classic "1.1.3"
    :exclusions [org.slf4j/slf4j-api]]
   [org.slf4j/jul-to-slf4j "1.7.13"]
   [org.slf4j/jcl-over-slf4j "1.7.13"]
   [org.slf4j/log4j-over-slf4j "1.7.13"]
   [org.clojure/tools.logging "0.3.1"]
   [org.clojure/tools.reader "1.0.0-alpha1"]]

  :main atompit.main
  :profiles {:dev {:source-paths #{"src" "dev"}
                   :resource-paths #{"target"}}
             :uberjar {:resource-paths #{"target/prod"}
                       :target-path "uber"
                       :aot [atompit.main]
                       :jvm-opts ["-Xmx2G"]}})
