(ns f
  (:import [java.util Date]))

(defn trunc-day [^java.util.Date date]
  (java.util.Date. (.getYear date) (.getMonth date) (.getDate date)))

(defn trunc-hour [^java.util.Date date]
  (java.util.Date. (.getYear date) (.getMonth date) (.getDate date)
    (.getHours date) 0))

