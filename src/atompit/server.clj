(ns atompit.server
  (:require [clojure.tools.logging      :as log]
            [com.stuartsierra.component :as c]
            [io.pedestal.http           :as http]

            [atompit.api :as api]
            [atompit.db :as db]))

(Thread/setDefaultUncaughtExceptionHandler
  (reify Thread$UncaughtExceptionHandler
    (uncaughtException [_ thread ex]
      (log/error ex "Uncaught exception on" (.getName thread)))))

(defrecord Web [port host]
  c/Lifecycle
  (start [this]
    (let [service (-> api/service
                    (assoc
                      :env :dev
                      ::http/port (Integer/parseInt port)
                      ::http/host host)
                    http/default-interceptors
                    (update-in [::http/interceptors] conj
                      (api/request-inject :db #(db/db db/conn)))
                    http/dev-interceptors)
          server  (http/create-server service)]
      (http/start server)
      (log/info "Started server on" (str host ":" port))
      (assoc this :server server)))

  (stop [this]
    (log/info "Stopping server on " (str host ":" port))
    (http/stop (:server this))
    (assoc this :server nil)))
