(ns atompit.main
  (:gen-class)

  (:require [clojure.java.io :as io]
            [clojure.string :as str]
            [clojure.tools.cli :as cli]
            [clojure.tools.nrepl.server :as nrepl]
            [clojure.tools.nrepl.middleware.session :as nrepl-session]
            [phoenix]
            [phoenix.core :as pc]
            [hawk.core :as hawk]
            [cemerick.piggieback :as piggie]
            [cider.nrepl :as cider]
            [refactor-nrepl.middleware :as refactor]

            [atompit.devbuild :as devbuild]))

(defn start-autoreload! []
  (hawk/watch!
    [{:paths ["src"]
      :filter (fn [_ {:keys [file]}]
                (and (.isFile file)
                  (re-find #".cljc?$" (.getName file))))
      :handler (fn [_ _]
                 ;; this binding prevents clojure.tools.namespace.repl blowing
                 ;; up with "Can't set!: *ns* from non-binding thread"
                 (binding [*ns* *ns*]
                   (phoenix/reload!)))}]))

(defn start-repl! [port]
  (doto (io/file ".nrepl-port")
    (spit port)
    (.deleteOnExit))
  (nrepl/start-server
    :port port
    :handler (->> (map resolve cider/cider-middleware)
               (into [#'refactor/wrap-refactor
                      #'piggie/wrap-cljs-repl])
               (apply nrepl/default-handler))))

(defn run-system! [config]
  (let [cfg (pc/load-config {:config-source config})]
    (phoenix/init-phoenix! config)
    (start-repl! (-> cfg :nrepl-port))
    (binding [*ns* *ns*]
      (phoenix/start!))))

(defn run-dev! [config]
  (let [cfg (pc/load-config {:config-source config})]
    (devbuild/start-sass! (-> cfg :sass :src) (-> cfg :sass :dst))
    (start-autoreload!)
    (run-system! config)
    (devbuild/start-figwheel!)))

(def cli-options
  [["-h" "--help" "show help"]
   ["-s" "--system NAME" "system configuration"
    :default "system.edn"]
   [nil "--dev" "start in development mode (with cljs compilation)"]
   [nil "--min" "compile CLJS in advanced mode"]])

(defn -main
  "Entry point."
  [& args]
  (let [{:keys [options arguments summary errors]}
        (cli/parse-opts args cli-options)]
    (cond
      errors
      (println "Errors:"
        (str/join "\n\t" errors))

      (:help options)
      (println summary)

      (:min options)
      (devbuild/cljs-build devbuild/prod-build)

      (:dev options)
      (run-dev! (io/resource (:system options)))

      :else
      (run-system! (io/resource (:system options))))))

