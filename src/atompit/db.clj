(ns atompit.db
  (:require [com.stuartsierra.component :as c]
            [datomic.api :as d]

            [f]))

(def q d/q)
(def db d/db)
(def transact d/transact)

(def uri (or (System/getenv "DATOMIC")
           "datomic:free://10.44.40.30:4334/atompit"))
(def conn (d/connect uri))


(def SCHEMA-ATTRS
  {:fulltext        {:db/fulltext true}
   :component       {:db/isComponent true}
   :unique-value    {:db/unique :db.unique/value}
   :unique-identity {:db/unique :db.unique/identity}
   :no-history      {:db/noHistory true}})

(defn create-attribute
   "Create a schema from an attribute definition vector:
    Example:
       (create-attribute [:story/title \"full title\" :string :one :fulltext])"
  [attr-schema]
  (let [[attr-ident attr-doc attr-type cardi & bits] attr-schema
        schema {:db/id                 (d/tempid :db.part/db)
                :db/ident              attr-ident
                :db/doc                attr-doc
                :db/valueType          (keyword "db.type" (name attr-type))
                :db/cardinality        (keyword "db.cardinality" (name cardi))
                :db/index              true
                :db.install/_attribute :db.part/db}]
    (if (seq bits)
      (apply merge schema
        ((apply juxt bits) SCHEMA-ATTRS))
      schema)))

(defn create [attrs]
  (mapv create-attribute attrs))

(def schema
  (create [[:chart/name "Chart name" :string :one]
           [:chart/query "Chart query" :string :one]
           [:chart/pos "Chart position" :long :one]

           [:order/id "Order id" :long :one :unique-identity]
           [:order/done-at "Order is finalized at this time" :instant :one]
           [:order/processing-cost "Order processing cost" :bigdec :one]
           [:order/user-id "Order user id" :long :one]
           [:order/items "Order items" :ref :many :component]

           [:item/sku-id "SKU id" :long :one]
           [:item/price "Price" :bigdec :one]
           [:item/quantity "Quantity" :long :one]]))

(defn trunc-day [^java.util.Date date]
  (java.util.Date. (.getYear date) (.getMonth date) (.getDate date)))

(defn edn-query->map [query]
  (loop [parsed {}, key nil, qs query]
    (if-let [q (first qs)]
      (if (keyword? q)
        (recur parsed q (next qs))
        (recur (update-in parsed [key] (fnil conj []) q) key (next qs)))
      parsed)))

(defn wrong-call? [clause]
  (and (list? clause)
    (not (empty? clause))
    (symbol? (first clause))
    (if-let [f-ns (namespace (first clause))]
      (not= f-ns "f")
      false)))

(defn validate [q]
  (let [qmap    (edn-query->map q)
        v-find  (every? false? (map wrong-call? (:find qmap)))
        v-where (every? false? (map (comp wrong-call? first) (:where qmap)))]
    (and v-find v-where)))

(comment
  (let [q '[:find ?day (count ?e) (count-distinct ?user) (sum ?line-price)
            :in $
            ^{type/date #inst "2015-11-30"} ?start
            ^{type/date #inst "2015-12-02"} ?end

            :where
            [?e :order/done-at ?time]

            [(> ?time ?start)]
            [(< ?time ?end)]
            [(f/trunc-day ?time) ?day]

            [?e :order/user-id ?user]

            [?e :order/items ?i]
            [?i :item/price ?price]
            [?i :item/quantity ?quantity]
            [(* ?price ?quantity) ?line-price]]]
    (time
      (println
        (d/q q (d/db conn)
          #inst "2015-11-30" #inst "2015-12-06"))))

  (d/q '[:find
            ^axis/x ?hour
            ^"Orders count" (count ?e)
            ^"Buyers count" (count-distinct ?user)
            ^{axis/y2 "Revenue"} (sum ?line-price)

            :in $
            ^{type/date #inst "2015-11-27T00:00"} ?start
            ^{type/date #inst "2015-11-27T23:59"} ?end

            :where
            [?e :order/done-at ?time]

            [(> ?time ?start)]
            [(< ?time ?end)]
            [(f/trunc-hour ?time) ?hour]

            [?e :order/user-id ?user]

            [?e :order/items ?i]
            [?i :item/price ?price]
            [?i :item/quantity ?quantity]
         [(* ?price ?quantity) ?line-price]]
    (d/db conn) #inst "2015-11-27T00:00" #inst "2015-11-27T23:59")

  (d/q '[:find (count-distinct ?user)
         :in $ ?start ?end
         :where
         [?e :order/done-at ?time]
         [(> ?time ?start)]
         [(< ?time ?end)]
         [?e :order/user-id ?user]]
    (d/db conn) #inst "2015-11-30" #inst "2015-12-06"))
