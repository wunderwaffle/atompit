(ns atompit.devbuild
  (:require [clojure.java.io :as io]
            [me.raynes.conch.low-level :as sh]
            [hawk.core :as hawk]
            [com.stuartsierra.component :as c]
            [cljs.build.api :as cljs]
            [figwheel-sidecar.repl-api :as ra]))

(def dev-build
  {:id "dev"
   :source-paths ["src" "dev" "test"]
   :compiler {:output-to            "target/main.js"
              :output-dir           "target/main.out"
              :asset-path           "/static/main.out"
              :devcards             true
              :main                 'atompit.start
              :optimizations        :none
              :source-map           true
              :source-map-timestamp true}})

(def prod-build
  {:id "prod"
   :source-paths ["src"]
   :compiler {:output-to      "target/prod/main.js"
              :output-dir     "target/prod.out"
              :main           'atompit.fe
              :optimizations  :advanced
              :compiler-stats true}})

(defn sh [& args]
  (let [proc (apply sh/proc args)]
    (future (sh/stream-to proc :out (System/out)))
    (future (sh/stream-to proc :err (System/err)))
    proc))

(defn start-figwheel! []
  (ra/start-figwheel!
    {:figwheel-options {:open-file-command "figwheel-open"
                        :css-dirs ["target"]}
     :all-builds [dev-build]
     :build-ids ["dev"]})
  (ra/cljs-repl))

(defn start-sass! [src dst]
  (let [src-path (-> src io/resource .getPath)
        dst-path (-> (str "target/" dst) io/file .getAbsolutePath)
        compile  (fn [_ _]
                   (println "Compiling SCSS...")
                   (sh "sassc" "-m" src-path dst-path))]
    (hawk/watch!
      [{:paths ["resources/scss"]
        :filter (fn [_ {:keys [file]}]
                  (and (.isFile file)
                    (re-find #".scss$" (.getName file))))
        :handler compile}])
    (compile nil nil)))

(defn cljs-build [{:keys [source-paths compiler]}]
  (cljs/build (apply cljs/inputs source-paths) compiler))
