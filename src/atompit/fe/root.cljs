(ns atompit.fe.root
  (:require [javelin.core :as j]
            [rum.core :as rum]

            [atompit.fe.data :as data]
            [atompit.fe.handlers :as handlers]
            [atompit.fe.utils :as utils]
            [atompit.fe.chart :as chart]
            [atompit.fe.inputs :as inputs]
            [atompit.fe.edit :as edit]
            [atompit.fe.alerts :as alerts]
            [atompit.fe.about :as about]))

(rum/defcs ChartLine
  < (data/query-mixin :data
      (fn [chart]
        (j/cell= {:data (get-in data/db [:charts-data (:db/id chart)])
                  :args (get-in data/db [:charts-args (:db/id chart)])})))

    (data/query-mixin :edit
      (fn [chart]
        (j/cell= ((:editing data/db) (:db/id chart)))))

    {:wrap-render
     (fn [render-fn]
       (fn [state]
         (let [[chart]  (:rum/args state)
               chart-id (:db/id chart)
               query    (:chart/query chart)
               data     (:data state)
               args     (filter identity
                          (or (:args @data)
                              (utils/default-inputs query)))]
           (when (= (-> args count)
                   (-> chart :chart/query utils/query->inputs count))
             (handlers/load-data chart-id query args))
           (render-fn state))))}

  [{:keys [data edit]} chart]

  [:div.col-sm-12
   [:div.chart-wrapper
    [:div.chart-title {:style {:background-color (when @edit "#ccc")}}
     (:chart/name chart)
     [:a.pull-right
      {:href "#" :on-click #(do (.preventDefault %)
                                (handlers/toggle-editing (:db/id chart)))}
      "edit"]]

    [:div.chart-stage
     (if @edit
       (edit/ChartEdit chart)
       (when (:data @data)
         (chart/Chart
           (utils/transform-chart (:chart/query chart) (:data @data)))))]
    [:div.chart-notes
     (inputs/ChartInputs (:db/id chart) (:chart/query chart))]]])

(rum/defcs AddLine < (rum/local false :hover)
  [{:keys [hover]}]

  [:div.col-sm-12 {:on-mouse-over #(reset! hover true)
                   :on-mouse-out #(reset! hover false)
                   ;; TBH, it's not "disabled", there is no interface for that,
                   ;; but we decided against working on adding new charts so
                   ;; that we could focus on more pressing challenges
                   :on-click #(alerts/show :warning "Adding charts is disabled for demo")}
   [:div.chart-wrapper {:style {:background-color (when-not @hover "transparent")
                                :border (when-not @hover 0)}}
    [:div.chart-title {:style {:opacity (if @hover 0.5 0)}}
     "Best chart ever"]

    [:div.chart-stage {:style {:display "flex"
                               :justify-content "center"
                               :align-items "center"}}
     [:div {:style {:font-size "20em"
                    :cursor "default"
                    :margin-top "-0.2em"
                    :color (if @hover "#23527c" "#999")}}
      "+"]]

    [:div.chart-notes {:style {:opacity (if @hover 0.5 0)}}
     "Add new chart and be happy for days!"]]])

(rum/defcs Dashboard
  < (data/query-mixin :charts
      #(j/cell= (:charts data/db)))

  {:will-mount (fn [state]
                 (handlers/load-charts)
                 state)}

  [{:keys [charts]}]

  [:div.container-fluid
   (for [chart @charts]
     [:div.row {:key (:chart/pos chart)}
      (ChartLine chart)])

   [:div.row (AddLine)]])

(rum/defcs Navbar < (rum/local true :collapsed)
  [{:keys [collapsed]} section]
  [:div.navbar.navbar-inverse.navbar-static-top
   {:role "navigation" :style {:margin "-60px -20px 20px -20px"}}
   [:div.container-fluid
    [:div.navbar-header
     [:button.navbar-toggle {:on-click #(swap! collapsed not)}
      [:span.sr-only "Toggle navigation"]
      [:span.icon-bar]
      [:span.icon-bar]
      [:span.icon-bar]]

     [:a.navbar-brand {:href "/"}
      [:span.glyphicon.glyphicon-blackboard]]
     [:a.navbar-brand {:href "/"} "ATOMPIT"]]

    [:div.navbar-collapse.collapse {:class (when-not @collapsed "in")}
     [:ul.nav.navbar-nav.navbar-left
      [:li [:a {:href "#"
                :on-click (fn [e] (.preventDefault e)
                            (reset! section :home))}
            "Home"]]
      [:li [:a {:href "#"
                :on-click (fn [e] (.preventDefault e)
                            (reset! section :about))}
            "About"]]]]]])

(rum/defcs Root < (rum/local :home :section)
  [{:keys [section]}]

  [:div
   (Navbar section)

   (alerts/Alerts)

   (case @section
     :home (Dashboard)
     :about (about/About))])
