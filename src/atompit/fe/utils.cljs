(ns atompit.fe.utils
  (:require [cljs.tools.reader.edn :as edn]
            [cljs.tools.reader.reader-types :as rt]))

(defn toggle [set value]
  ((if (set value) disj conj) set value))

(defn read-edn [string]
  (let [reader (rt/string-push-back-reader (.trim string))
        result (edn/read
                 {:readers {'inst #(js/Date. (.replace % "T" " "))}
                  :eof nil}
                 reader)]
    (if (rt/peek-char reader)
      (let [pos (-> reader .-rdr .-s-pos)
            s   (-> reader .-rdr .-s)]
        (throw (js/Error. (str "Couldn't parse string to end, remainder: "
                            (.slice s pos)))))
      result)))

;; https://github.com/tonsky/datascript/blob/src/datascript/parser.cljc#L672
(defn edn-query->map [query]
  (loop [parsed {}, key nil, qs query]
    (if-let [q (first qs)]
      (if (keyword? q)
        (recur parsed q (next qs))
        (recur (update-in parsed [key] (fnil conj []) q) key (next qs)))
      parsed)))

(defn query->map [query-string]
  (-> query-string
    read-edn
    edn-query->map))

(defn find->tag [x]
  "You can tag values like that:
   [:find ^axis/x ?value]             ;; 'axis/x
   [:find ^{axis/y2 \"Name\"} ?value] ;; 'axis/y2
   [:find ^\"Name\" ?value]           ;; 'axis/y"
  (let [meta-tag (:tag (meta x))]
    (or (when (symbol? meta-tag) meta-tag)
        (some-> (meta x) first key)
        'axis/y)))

(defn find->name [x]
  "You can name values like that:
   [:find ^{axis/y2 \"Name\"} ?value] ;; Name
   [:find ^\"Name\" ?value]           ;; Name
   [:find ?value]                     ;; ?value"
  (let [tag (find->tag x)]
    (or (get (meta x) tag)
        (when (string? tag) tag)
        (str x))))

(defn get-x-type [findclause chart-data]
  (let [pos-tags  (map-indexed (fn [i find] [(find->tag find) i]) findclause)
        x-pos     ((into {} pos-tags) 'axis/x)
        x-isdate  (instance? js/Date (get-in chart-data [0 x-pos]))]
    (cond
      x-isdate :timeseries
      x-pos    :category
      :else    :indexed)))

(defn name-charts [findclause chart-data]
  (let [transformed (apply mapv vector chart-data)
        names       (map find->name findclause)]
    (zipmap names transformed)))

(defn transform-chart [query-string chart-data]
  (let [query      (query->map query-string)
        findclause (:find query)
        x-type     (get-x-type findclause chart-data)
        x-axis     (first (filter #(= 'axis/x (find->tag %)) findclause))
        x-name     ((fnil find->name "") x-axis)

        y2finds    (filter #(= 'axis/y2 (find->tag %)) findclause)
        y2axes     (zipmap (map find->name y2finds) (repeat "y2"))]

    {:json (name-charts findclause chart-data)
     :x    x-name
     :axes y2axes
     :axis {:x {:type x-type}
            :y2 {:show (not (empty? y2axes))}}
     :type :area}))

(defn in->default [x]
  (some-> (meta x) first val))

(defn in->type [x]
  (some-> (meta x) first key))

(defn query->inputs [query-string]
  (let [query    (query->map query-string)
        inclause (:in query)
        inputs   (filter #(and (symbol? %) (= "?" (subs (name %) 0 1))) inclause)]
    inputs))

(defn default-inputs [query-string]
  (let [inputs   (query->inputs query-string)
        defaults (map in->default inputs)]
    defaults))
