(ns atompit.fe.edit
  (:require [clojure.string :as str]
            [reforms.rum :as f]
            [reforms.validation :include-macros true :as v]
            [rum.core :as rum]

            [atompit.fe.handlers :as handlers]
            [atompit.fe.utils :as utils]))

(defn edn-string? [korks error-message]
  (fn [cursor]
    (try
      (-> (get-in cursor korks)
        utils/read-edn)
      (catch js/Error e e
             (v/validation-error [korks] (aget e "message"))))
    nil))

(defn proper-query? [korks]
  (fn [cursor]
    (let [q       (utils/query->map (get-in cursor korks))
          unknown (keys (dissoc q :where :find :in :with))]
      (cond
        (nil? (:find q))
        (v/validation-error [korks]
          "No :find clause, what do you want to draw?")

        (nil? (:where q))
        (v/validation-error [korks]
          "No :where clause, how do I find your data?")

        (not (empty? unknown))
        (v/validation-error [korks]
          (str "Some unknown clauses in the query: " (str/join ", " unknown)))))))

(defn short-circuit-v [& validators]
  (fn [cursor]
    (loop [error nil
           [validator & rest] validators]
      (if (or error (nil? validator))
        error
        (recur (validator cursor) rest)))))

(defn save-chart! [save? data ui-state]
  (when (v/validate! data ui-state
          (v/present [:chart/name]
            "Name is required")
          (v/present [:chart/query]
            "Query is required")
          (short-circuit-v
            (edn-string? [:chart/query]
              "Malformed query: likely I couldn't parse string to the end,
               but maybe something else is to blame as well")
            (proper-query? [:chart/query])))
    (if save?
      (handlers/save-chart @data)
      (handlers/replace-chart @data))
    (handlers/toggle-editing (:db/id @data))))

(rum/defc ChartForm < rum/cursored rum/cursored-watch
  [data ui-state]
  (v/form ui-state
    {:on-submit #(save-chart! true data ui-state)}

    (vec ;; that's a way to fight reforms, they're trying to revolt against me
      [:div.row
       [:div.col.col-sm-6
        (v/textarea {:style {:height 300}} data [:chart/query])]

       [:div.col.col-sm-6
        (v/text "Name" data [:chart/name])

        (f/form-buttons
          (f/button-primary "Save" #(save-chart! true data ui-state))
          #_ (f/button-default "Try" {:style {:margin-right 6 :margin-left 6}}
            #(save-chart! false data ui-state))
          (f/button-default "Cancel" #(handlers/toggle-editing (:db/id @data))))]])))

(rum/defcs ChartEdit
  < (rum/local {} :form)
    (rum/local {} :ui-state)
    {:will-mount (fn [state]
                   (let [[chart] (:rum/args state)]
                     (reset! (:form state) chart))
                   state)}

  [{:keys [form ui-state]} chart]

  (ChartForm form ui-state))
