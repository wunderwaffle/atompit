(ns atompit.fe.about
  (:require [rum.core :as rum]))

(rum/defc About []
  [:div.container
   [:div.jumbotron {:style {:position "relative"
                            :background-color "rgba(0, 0, 0, 0.65)"}}
    [:div.background {:style {:position "absolute"
                              :top 0 :left 0 :right 0 :bottom 0
                              :opacity 0.8
                              :z-index -1
                              :background "url(/static/jumbo.jpg)"
                              :background-size "cover"}}]
    [:div.container {:style {:color "#eee"}}
     [:h1 "Atompit"]
     [:p "Atompit looks in your Datomic database and shows you your data.
         Got an interesting question? We give you all the power of Datalog,
         and render charts based on that."]]]

   [:p "For the ClojureCup demo we loaded Datomic with generated data that
         models e-commerce shop. We also disabled creating and saving charts to
         database, so everyone can fiddle with them, but noone could delete them
         or make a demo completely unusable. \"Save\" works only in your
         browser. Also, when you plan to run big queries, please take in
         consideration that demo server isn't very beefy, so be patient while
         waiting for your results :)"]

   [:h2 "Supported syntax"]
   [:p "We support sandboxed "
       [:a {:href "http://docs.datomic.com/query.html"} "Datomic query"]
       " syntax with small extensions. You can put additional information on inputs "
       [:code ":in"] " and outputs " [:code ":find"] " using Clojure metadata."]

   [:h3 "Inputs"]
   [:p "You can specify default input value and its type like this:"]
   [:p [:code ":in $ " [:strong "^{type/number 50}"] " " [:i "?limit"]]
       " - for input " [:code "?limit"] " use " [:code "50"] "as a default value,
       with type " [:code "type/number"] "."]
   [:p [:code ":in $ " [:strong "^{type/date #inst \"2015-11-29\"}"] " " [:i "?start"]]
       " - for input " [:code "?start"] " use " [:code "#inst \"2015-11-29\""]
       "as a default value, with type " [:code "type/date"] ". You can see how
       reader tags can be used to specify default datetime values."]
   [:p "Atompit recognizes three types for now: " [:code "number"] ", "
       [:code "date"] " and " [:code "text"] ", and will use them for query
       inputs validation and parsing."]

   [:h3 "Outputs"]
   [:p "For example, if we have an output that gives us datetimes - let's call it a " [:code "?day"] ", and we want to use that as an X axis, we can just mark it with " [:code "axis/x"] ", so it'll look like this:" [:code ":find ^axis/x ?day"] ". For further satisfaction, Atompit will detect that it is a datetime and will display meaningful tick hints."]
   [:p "Additionally, you can put some outputs on another Y axis, which is done in the same way with " [:code "axis/y2"]]
   [:p "And then we have another output that counts something - " [:code "(count ?e)"] ". That's not bad, it's possible to go look at a query and understand, but what if you want to display you chart to somebody? We want to name that output, and it can be done with name as a metadata: " [:code "^\"Orders count\" (count ?e)"]]
   [:p "It's even possible to combine these powers and simultaneously attach an output to another Y axis and give it some meanigful name. Just put both of them in a map like this: " [:code "^{axis/y2 \"Revenue\"}"]]

   [:h3 "Custom functions"]
   [:p "Additionally to functions Datomic provides you in Datalog queries
        (their functions plus Clojure's core functions), we defined few custom
        ones:"]
   [:ul
    [:li [:code "trunc-day"] " - truncates given date to a day"]
    [:li [:code "trunc-hour"] " - truncates given date to an hour"]]])
