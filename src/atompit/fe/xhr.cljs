(ns atompit.fe.xhr
  (:require [cognitect.transit :as t]
            [clojure.string :as str]))

;; parse bigdec as number
(def tran-reader (t/reader :json {:handlers {"f" identity}}))
(def tran-writer (t/writer :json))

(def url-encode js/encodeURIComponent)
(def url-decode js/decodeURIComponent)

(defn- encode-val [k v]
  (str (url-encode (name k)) "=" (url-encode (str v))))

(defn- encode-vals [k vs]
  (->>
    vs
    (map #(encode-val k %))
    (str/join "&")))

(defn- encode-param [[k v]]
  (if (coll? v)
    (encode-vals k v)
    (encode-val k v)))

(defn generate-query-string [params]
  (->>
    params
    (map encode-param)
    (str/join "&")))

(defn xhr [{:keys [url method data type handler error]}]
  (let [xhr (js/XMLHttpRequest.)
        url (if (and (seq data) (= :get method))
              (str url "?" (generate-query-string data))
              url)]
    (.open xhr (name method) url true)

    (case type
      :form (.setRequestHeader xhr "Content-Type"
              "application/x-www-form-urlencoded")
      :transit (.setRequestHeader xhr "Content-Type"
                 "application/transit+json")
      nil)

    (aset xhr "onreadystatechange"
      #(when (= 4 (.-readyState xhr))
         (let [text   (.-responseText xhr)
               is-ok  (<= 200 (.-status xhr) 299)]
           (try
             (if is-ok
               (handler (t/read tran-reader text))
               (error xhr (t/read tran-reader text)))
             (catch js/Error e
               (error e text))))))

    (let [data (case type
                 :form (generate-query-string data)
                 :transit (t/write tran-writer data)
                 data)]
      (if (= :get method)
        (.send xhr)
        (.send xhr data)))
    xhr))
