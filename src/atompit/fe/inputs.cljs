(ns atompit.fe.inputs
  (:require [atompit.fe.handlers :as handlers]
            [atompit.fe.utils :as utils]
            [rum.core :as rum]))

(defn input-widget [input-type]
  (case input-type
    'type/number :number
    type/date    :datetime
                 :text))

(defn input->str [input-type input-value]
  (case input-type
    'type/date (.toISOString input-value)
               (str input-value)))

(defn str->input [input-type string]
  (case input-type
    'type/number (js/parseFloat string)
    type/date    (js/Date. string)
                 string))

(rum/defc InputsForm < rum/cursored rum/cursored-watch
  [chart-id default-data data ui-state]
  [:form.form-horizontal {:on-submit #(println "hello")}
   (map-indexed
     (fn [i [i-name i-type i-value]]
       [:div.form-group
        [:label.control-label.col-sm-2 {:for i-name}
         (str i-name " (" (if i-type (name i-type) i-type) ")")]
        [:div.col-sm-10
         [:input {:style {:width "100%"}
                  :id i-name
                  :type (input-widget i-type)
                  :value (input->str i-type i-value)
                  :on-change #(swap! data assoc-in [i 2]
                                (->> % .-target .-value (str->input i-type)))}]]])
     @data)

   [:div.form-group
    [:div.col-sm-offset-2.col-sm-10
     [:button.btn.btn-primary
      {:type "submit"
       :on-click (fn [e]
                   (.preventDefault e)
                   (handlers/save-args chart-id (map #(nth % 2) @data)))}
      "Apply"]
     [:button.btn.btn-default
      {:on-click #(do (.preventDefault %)
                      (reset! data default-data))}
      "Reset"]]]])

(rum/defcs *ChartInputs
  < (rum/local {} :form)
    (rum/local {} :ui-state)
    {:will-mount (fn [state]
                   (let [[chart-id data] (:rum/args state)]
                     (reset! (:form state) data))
                   state)}

  [{:keys [form ui-state]} chart-id inputs]

  (InputsForm chart-id inputs form ui-state))

(rum/defc ChartInputs
  [chart-id query-string]
  (let [inputs (utils/query->inputs query-string)
        names  (map str inputs)
        types  (map utils/in->type inputs)
        values (map utils/in->default inputs)
        data   (apply mapv vector [names types values])]

    (*ChartInputs chart-id data)))
