(ns atompit.fe.data
  (:require [javelin.core :as j :refer [cell]]
            [rum.core :as rum]))

(def init-data
  {:charts      []
   :charts-data {}
   :charts-args {}
   :editing     #{}
   :alerts      []})

(defonce db (cell init-data))

(defn query-mixin
  [key cell-fn]
  (letfn [(create [state]
            (let [ref  (apply cell-fn (:rum/args state))
                  comp (:rum/react-component state)]
              (add-watch ref key
                (fn [_ _ _ _]
                  (rum/request-render comp)))
              (assoc state key ref)))

          (destroy [state]
            (let [ref (get state key)]
              (remove-watch ref key)
              (j/destroy-cell! ref))
            (dissoc state key))]

    {:will-mount     create
     :will-unmount   destroy
     :transfer-state (fn [old new]
                       (if (= (:rum/args old) (:rum/args new))
                         (assoc new key (old key))
                         (do
                           (destroy old)
                           (create new))))}))
