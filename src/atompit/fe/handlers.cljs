(ns atompit.fe.handlers
  (:require [atompit.fe.data :as data]
            [atompit.fe.utils :as utils]
            [atompit.fe.xhr :as xhr]))

(defn toggle-editing [id]
  (swap! data/db update :editing utils/toggle id))

;; charts

(defn load-charts+ [data]
  (swap! data/db assoc :charts (sort-by :chart/pos data)))

(defn load-charts- [err data]
  (println err data))

(defn load-charts []
  (xhr/xhr {:url "/api/query"
            :method :post
            :data {:q '[:find [(pull ?e [*]) ...]
                        :where [?e :chart/name]]}
            :type :transit
            :handler load-charts+
            :error load-charts-}))

(defn replace-chart [data]
  (swap! data/db update :charts
    (partial mapv #(if (= (:db/id data) (:db/id %)) data %))))

(defn save-chart+ [old-data chart]
  #_ (load-charts)
  (replace-chart chart))

(defn save-chart- [old-data err data]
  (println err data))

(defn save-chart [data]
  (xhr/xhr {:url "/api/chart"
            :method :post
            :data data
            :type :transit
            :handler (partial save-chart+ data)
            :error (partial save-chart- data)}))

(defn save-args [chart-id args]
  (swap! data/db assoc-in [:charts-args chart-id] args))

;; data

(defn load-data+ [chart-id data]
  (swap! data/db assoc-in [:charts-data chart-id] data))

(defn load-data- [chart-id err data]
  (println chart-id err data))

(def -data-cache (atom {}))

(defn load-data [chart-id query args]
  (if-let [cached (get -data-cache [query args])]
    (load-data+ chart-id cached)
    (xhr/xhr {:url "/api/query"
              :method :post
              :data {:q query :args args}
              :type :transit
              :handler (fn [data]
                         (swap! -data-cache assoc [query args] data)
                         (load-data+ chart-id data))
              :error (partial load-data- chart-id)})))

