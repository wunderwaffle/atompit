(ns atompit.fe.chart
  (:require [rum.core :as rum]
            [cljsjs.c3]))

(defn state->el [state]
  (-> state :rum/react-component js/ReactDOM.findDOMNode))

(rum/defc Chart
  < {:did-mount
     (fn [state]
       (let [[chart-data] (:rum/args state)
             data         (clj->js chart-data)
             c3chart      (js/c3.generate #js {:data data
                                               :axis (aget data "axis")
                                               :bindto (state->el state)})]
         (assoc state :chart c3chart)))

     :transfer-state
     (fn [old-state state]
       (let [[chart-data-old] (:rum/args old-state)
             [chart-data-new] (:rum/args state)
             data             (clj->js chart-data-new)
             c3chart          (:chart old-state)]

         (cond-> c3chart
           (not= chart-data-old chart-data-new)
           (.load data)

           (not= (:groups chart-data-old) (:groups chart-data-new))
           (.groups (aget data "groups")))

         (assoc state :chart c3chart)))

     :will-unmount
     (fn [state]
       (.destroy (:chart state))
       (dissoc state :chart))}

  [chart-data]

  [:div])
