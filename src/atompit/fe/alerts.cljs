(ns atompit.fe.alerts
  (:require [javelin.core :as j]
            [rum.core :as rum]

            [atompit.fe.data :as data]))

(rum/defcs Alerts
  < (data/query-mixin :alerts
      #(j/cell= (:alerts data/db)))

  [{:keys [alerts]}]

  [:div {:style {:position "fixed"
                 :top 50
                 :left 20
                 :right 20
                 :z-index 10}}
   (for [[id type message] @alerts]
     [:div.alert {:class (str "alert-" (name type))
                  :role "alert"
                  :key id}
      message])])

(defn show [type message]
  {:pre [(#{:success :info :warning :danger} type)]}
  (let [id (rum/next-id)]
    (swap! data/db update :alerts conj [id type message])
    (js/setTimeout
      (fn [] (swap! data/db update :alerts (partial remove #(= id (first %)))))
      3000)))
