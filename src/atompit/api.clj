(ns atompit.api
  (:require [clojure.edn :as edn]
            [clojure.tools.logging :as log]
            [io.pedestal.http :as http]
            [io.pedestal.http.body-params :as body-params]
            [io.pedestal.http.route.definition :refer [defroutes]]
            [io.pedestal.interceptor :as ic]
            [ring.util.response :as response]
            [ring.middleware.head :as head]
            [hiccup.core :refer [html]]

            [atompit.db :as db]))

(defn request-inject [id get-value]
  (ic/interceptor {:name (str "inject-" (name id))
                   :enter #(assoc-in % [:request id] (get-value))}))

(defn index-template [& body]
  (html
    "<!DOCTYPE html>\n"
    [:head
     [:title "ATOMPIT"]
     [:meta {:charset "utf-8"}]
     [:link {:rel "stylesheet" :href "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"}]
     [:link {:rel "stylesheet" :href "https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"}]
     [:link {:rel "stylesheet" :href "/static/cljsjs/c3/common/c3.css"}]
     [:link {:rel "stylesheet" :href "/static/dashboard.css"}]]
    [:body.application
     body]))

(defn index [req]
  {:status 200
   :headers {"Content-Type" "text/html"}
   :body (index-template
           [:div#app]
           [:script {:src "/static/main.js"}]
           [:script "atompit.fe.main()"])})

(defn devcards [req]
  {:status 200
   :headers {"Content-Type" "text/html"}
   :body (index-template
           [:div#app]
           [:script {:src "/static/main.out/goog/base.js"}]
           [:script {:src "/static/main.out/cljs_deps.js"}]
           [:script "goog.require('atompit.devcards')"])})


(defn serve-static
  [{{:keys [path]} :path-params :as request}]
  (or (-> (response/resource-response path)
          (head/head-response request))
    {:status 404}))

(def hard-cache (atom {}))

(defn query-db
  [{db :db
    {q :q args :args} :transit-params}]
  (let [q      (if (string? q) (edn/read-string q) q)
        valid? (db/validate q)]
    (cond
      (not valid?)
      {:status 400
       :body ["Bad query"]}

      (get @hard-cache [q args])
      {:status 200
       :body (get @hard-cache [q args])}

      :else
      {:status 200
       :body (let [res (vec (apply db/q q db args))]
               (swap! hard-cache assoc [q args] res)
               res)})))

(defn save-chart
  [{db :db
    chart :transit-params}]
  (let [chart (merge {:db/id #db/id[:db.part/user]} chart)
        id    (:db/id chart)
        tran  nil #_@(db/transact db/conn [chart])
        id    (get-in tran [:tempids id] id)]
    {:status 200
     :body (assoc chart :db/id id)}))

(defroutes routes
  [[["/"
     ["/" {:get index}]
     ["/cards" {:get devcards}]

     ["/static/*path" {:get [:static-get serve-static]
                       :head [:static-head serve-static]}]

     ["/api" ^:interceptors [http/transit-body
                             (body-params/body-params)]
      ["/query" {:post query-db}]
      ["/chart" {:post save-chart}]]]]])

(def service
  {::http/routes routes
   ::http/type   :immutant
   ::http/join?  false
   ::http/port   0})
