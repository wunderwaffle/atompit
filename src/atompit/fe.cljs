(ns atompit.fe
  (:require [rum.core :as rum]

            [atompit.fe.data :as data]
            [atompit.fe.root :as root]))

(enable-console-print!)

(set! js/window.pr pr-str)
(set! js/window.to clj->js)
(set! js/window.from js->clj)
(set! js/window.deref deref)

(defonce target (atom {:el nil
                       :comp nil}))

(defn trigger-render []
  (let [{:keys [comp el]} @target]
    (cond
      comp (rum/request-render comp)
      el   (swap! target assoc :comp
             (rum/mount (root/Root) el)))))

(defn ^:export main []
  (let [el (js/document.getElementById "app")]
    (swap! target assoc :el el)
    (trigger-render)))
